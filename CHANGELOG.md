# Changelog

## 0.2.6 - 2021-10-12
* Improved error handling / reporting in FPE/eFPE processing

## 0.2.5 - 2021-09-29
* Added FPE/eFPE capability

## 0.2.4 - 2021-01-26
* Update to bouncy castle 1.68

## 0.2.3 - 2020-10-28
* Change to MIT license

## 0.2.2 - 2020-10-14
* Added explicit commands for Windows Operating System
* Added java version into API call

## 0.2.1 - 2020-09-29
* Initial Version
